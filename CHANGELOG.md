# Changelog

## 0.0.1

- Initial version, created by Stagehand

## 0.0.2

- webhook_handler_secrets.yaml auto creating

## 0.0.3

- fix

## 0.0.4

- fix

## 0.0.5

- README improved

## 0.0.6

- async run process