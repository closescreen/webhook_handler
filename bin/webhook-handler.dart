import 'dart:io';

/// A simple example showing how to add routes to Jaguar. Also shows how to
/// use serve JSON responses.
import 'package:jaguar/jaguar.dart';
import 'package:yaml/yaml.dart';

main() async {
  final fn = 'webhook_handler_secrets.yaml';
  var f = File(fn);
  if (!f.existsSync()) {
    f.writeAsStringSync('''
listen:
  # change me if need:
  host: 0.0.0.0
  # change me if need:
  port: 8087
  # change me for make it more secure:
  path: /release_key_word
run:
  # change if need:
  script: ./webhook-script.sh
  '''
          .trim(),flush: true);
    print('$fn created in current directory. Change it and run me again.');
    exit(2);
  }
  var yaml = f.readAsStringSync();
  var secrets = loadYaml(yaml) as YamlMap;

  if (!secrets.containsKey('run')) throw '$fn not contains "run"';
  if (!secrets['run'].containsKey('script'))
    throw '$fn "run" not contains "script"';

  var script = secrets['run']['script'];

  if (!secrets.containsKey('listen')) throw '$fn not contains "listen"';
  if (!secrets['listen'].containsKey('path'))
    throw '$fn "listen" not contains "path"';
  var path = secrets['listen']['path'];

  if (!secrets['listen'].containsKey('host'))
    throw '$fn "listen" not contains "host"';
  if (!secrets['listen'].containsKey('port'))
    throw '$fn "listen" not contains "port"';
  final server = Jaguar(
      address: secrets['listen']['host'], port: secrets['listen']['port']);

  server.get('/', (_) => 'Webhook handler.');

  handler(Context ctx) async {
    server.log.info('REQUEST: ${ctx.req.body}');

    try {
      var res = await Process.run(script, []);
      return {'stdout': res.stdout, 'stderr': res.stderr};
    } catch (e) {
      return {'error': e};
    }
  }

  server.post(path, handler, mimeType: MimeTypes.json);
  server.get(path, handler, mimeType: MimeTypes.json);

  server.log.onRecord.listen(print);

  await server.serve(logRequests: true);
}
