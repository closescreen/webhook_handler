# webhook handler

- `pub global activate webhook_handler`
- Run `webhook_handler` from directory where you will run webhook_handler later.
- webhook_handler_secrets.yaml will be created. 
- Change webhook_handler_secrets.yaml file in the current directory.
- Change the webhook-script.sh file. It will do some usefull job for you when webhook request coming.
- Run `webhook_handler` again
- Send GET or POST request according to webhook_handler_secrets.yaml to the server for run webhook-script.sh

